package com.mahaswami.training2019.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.mahaswami.training2019.jpa.model.Book;
import com.mahaswami.training2019.jpa.model.BookCategory;
import com.mahaswami.training2019.jpa.model.BookSales;
import com.mahaswami.training2019.jpa.model.Publisher;

public class BookService {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("Publisher");
	EntityManager em = emf.createEntityManager();
	EntityTransaction tx = em.getTransaction();
	
	public Book create(Book book){
		tx.begin();
		em.persist(book);
		tx.commit();
		return book;
	}

	public BookCategory create(BookCategory bc){
		tx.begin();
		em.persist(1);
		tx.commit();
		return bc;
	}

	public BookSales create(BookSales bs){
		tx.begin();
		em.persist(2);
		tx.commit();
		return bs;
	}

	public Publisher create(Publisher pb){
		tx.begin();
		em.persist(23);
		tx.commit();
		return pb;
	}
}
