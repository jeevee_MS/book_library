package com.mahaswami.training2019.jpa.model;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;
import javax.persistence.NamedQuery;

@Entity
@Table(name = "Book_List")
@NamedQuery(query = "Select b from Book b", name = "all_books")
@NamedQuery(query = "Select b from Book b where b.id = :id", name = "find_book_by_id")
@NamedQuery(query = "Select b from Book b where b.isbn = :isbn", name = "find_book_by_isbn")


public class Book implements Serializable {
	@Id  @GeneratedValue
	private long id;
	private String isbn;
	private String title;
	private String author;
	private int commission4Sales;
	private int date;
	private int purchaseprice;
	private int quantityavailable;
	private int salesprice;
	private int minimumInventoryQuantity;
	private static final long serialVersionUID = 1L;

	@ManyToOne
	private BookCategory bookcategory;

	@ManyToOne
	private BookSales bookSales;

	@ManyToOne
	private Publisher publisher;

	public Book() {
		super();
	}   
  
	public Book(String isbn, String title, String author, int date, int salesprice, int minimumInventoryQuantity,
				int purchaseprice, int quantityavailable, int commission4Sales) {
		super();
		this.isbn= isbn;
		this.title= title;
		this.author = author;
		this.date = date;
		this.purchaseprice = purchaseprice;
		this.quantityavailable = quantityavailable;
		this.salesprice = salesprice;
		this.minimumInventoryQuantity = minimumInventoryQuantity;
		this.commission4Sales = commission4Sales;
	}   
	
	public long getId() {
		return this.id;
	}
 
	public String getIsbn() {
		return this.isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}   
  
	public String getTheTitle() {
		return this.title;
	}

	public void setTheTitle(String title) {
		this.title = title;
	}

	public String getAuthor() { return author; }

	public void setAuthor(String author) { this.author = author; }

	public int getCommission4Sales() { return commission4Sales; }

	public void setCommission4Sales(int commission4Sales) { this.commission4Sales = commission4Sales; }

	public int getDate() { return date; }

	public void setDate(int date) { this.date = date; }

	public int getPurchaseprice() { return purchaseprice; }

	public void setPurchaseprice(int purchaseprice) { this.purchaseprice = purchaseprice; }

	public int getQuantityavailable() { return quantityavailable; }

	public void setQuantityavailable(int quantityavailable) { this.quantityavailable = quantityavailable; }

	public int getSalesprice() { return salesprice; }

	public void setSalesprice(int salesprice) { this.salesprice = salesprice; }

	public int getMinimumInventoryQuantity() { return minimumInventoryQuantity; }

	public void setMinimumInventoryQuantity(int minimumInventoryQuantity) { this.minimumInventoryQuantity = minimumInventoryQuantity; }

   
}
