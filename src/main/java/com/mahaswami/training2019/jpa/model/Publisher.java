package com.mahaswami.training2019.jpa.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Publisher")


public class Publisher implements Serializable {
	@Id  @GeneratedValue
	private long id;
	private String publisherName;
	private int outstandingAmount;
	private int totalSalesAmount;
	private static final long serialVersionUID = 1L;

	public Publisher() {
		super();
	}

	public Publisher(String publisherName, int totalsalesAmount, int outstandingAmount) {
		super();
		this.publisherName= publisherName;
		this.totalSalesAmount = totalsalesAmount;
		this.outstandingAmount = outstandingAmount;
		}
	
	public long getId() {
		return this.id;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public int getOutstandingAmount() {
		return outstandingAmount;
	}

	public void setOutstandingAmount(int outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}

	public int getTotalSalesAmount() {
		return totalSalesAmount;
	}

	public void setTotalSalesAmount(int totalSalesAmount) {
		this.totalSalesAmount = totalSalesAmount;
	}
   
}
