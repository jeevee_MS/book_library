package com.mahaswami.training2019.jpa.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Book Category")


public class BookCategory implements Serializable {
	@Id  @GeneratedValue
	private long id;
	private long serialnumber;
	private String BookCategory;
	private static final long serialVersionUID = 1L;

	public BookCategory() {
		super();
	}

	public BookCategory(String BookCategory) {
		super();
		this.BookCategory = BookCategory;
	}   
	
	public long getId() {
		return this.id;
	}

	public String getBookCategory() {
		return BookCategory;
	}

	public void setBookCategory(String bookCategory) {
		BookCategory = bookCategory;
	}

	public long getSerialnumber() {
		return serialnumber;
	}

	public void setSerialnumber(long serialnumber) {
		this.serialnumber = serialnumber;
	}

}
