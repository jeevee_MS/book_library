package com.mahaswami.training2019.jpa.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Book_Sales_Monitor")


public class BookSales implements Serializable {

	@Id  @GeneratedValue
	private long id;
	private int salesdate;
	private String bookTitle;
	private String salesClerkName;
	private int salesAmount;
	private static final long serialVersionUID = 1L;

	public BookSales() {
		super();
	}

	public BookSales(int salesAmount, String bookTitle, String salesClerkName, int salesdate) {
		super();
		this.salesAmount = salesAmount;
		this.bookTitle = bookTitle;
		this.salesClerkName= salesClerkName;
		this.salesdate = salesdate;
		}

	public long getId() {
		return this.id;
	}

	public int getSalesdate() {
		return salesdate;
	}

	public void setSalesdate(int salesdate) {
		this.salesdate = salesdate;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getSalesClerkName() {
		return salesClerkName;
	}

	public void setSalesClerkName(String salesClerkName) {
		this.salesClerkName = salesClerkName;
	}

	public int getSalesAmount() {
		return salesAmount;
	}

	public void setSalesAmount(int salesAmount) {
		this.salesAmount = salesAmount;
	}


}
